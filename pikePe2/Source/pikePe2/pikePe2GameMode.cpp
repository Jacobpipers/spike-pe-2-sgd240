// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "pikePe2.h"
#include "pikePe2GameMode.h"
#include "pikePe2HUD.h"
#include "pikePe2Character.h"

ApikePe2GameMode::ApikePe2GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ApikePe2HUD::StaticClass();
}
