// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "pikePe2GameMode.generated.h"

UCLASS(minimalapi)
class ApikePe2GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ApikePe2GameMode();
};



