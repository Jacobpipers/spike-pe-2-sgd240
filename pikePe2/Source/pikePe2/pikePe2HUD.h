// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "pikePe2HUD.generated.h"

UCLASS()
class ApikePe2HUD : public AHUD
{
	GENERATED_BODY()

public:
	ApikePe2HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

