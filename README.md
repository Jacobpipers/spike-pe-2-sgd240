# Spike Report

## SPIKE PE 2 - Collision and Trace C++

### Introduction

We want to understand the capabilities of the Physics Engine that Unreal uses. We should begin by looking at the basics of all physics engines – Collision and Raycasting!


### Goals

* The Spike Report should answer each of the Gap questions

Re-build the scenario from Spike PE-1 in C++, and add the following:

*	Create a Target (blueprint) class:
	*	They should be animated to move over time (Spline Components or Timelines may be helpful)
*	Create a demo level with several targets behind the translucent barrier from Spike PE-1
*	Set up each target to be made up from different materials
	*	Wood/Stone/Metal
	*	Ask a Graphics Programmer for the Materials
	*	You will also need to create a Physics Material and apply it within the graphics material
*	Add functionality so that, depending on the material hit, you apply different effects (sound/decal/particle/etc.).


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [TimeLine](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/Timelines/)
* [Physical Materials User Guide
](https://docs.unrealengine.com/latest/INT/Engine/Physics/PhysicalMaterials/PhysMatUserGuide/#creation)
* [Line Trace Single Channel](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Engine/UWorld/LineTraceSingleByChannel/index.html)
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

1. Create new Project Ussing C++ FPS Template
2. Change FPS code in Character Class- Remove Gun Code, Replace with Laser Code.
	* Check for a hit
		
		```c++
		
		bool bIsHit = World->LineTraceSingleByChannel(FireHit, StartLocation, EndLocation, ECC_Visibility, FireTraceParams);
		
		```
		
	* Check What Physics material was hit

		```c++
		
		UPhysicalMaterial* PhysicalMtl = FireHit.PhysMaterial.Get();
		
		```

		* In Character.h Define Surface types as seen below 
			
			```c++
			
			#define SURFACE_Wood SurfaceType1
			#define SURFACE_Stone SurfaceType2
			#define SURFACE_Metal SurfaceType3
			
			```

		* Apply decal and sound based on Phys Material
		
			```c++

			if (PhysicalMtl->SurfaceType == SURFACE_Wood)
			{
				if (LaserSoundList[0] != NULL)
				{
					LaserSound = LaserSoundList[0];
				}
				if (DecalMaterialList[0])
				{
					DecalMaterial = DecalMaterialList[0];
				}
				else {
					DecalMaterial = NULL;
				}
			} 

			```

		* Add [Emmiter Spawn Call](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Kismet/UGameplayStatics/SpawnEmitterAtLocation/2/index.html) Passing the hit info and refrence to Decal Material.
		* Add [Decal Attach Call](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Kismet/UGameplayStatics/SpawnDecalAttached/index.html). Pass hit info from cast.
		* Add [Sound At Location call](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Kismet/UGameplayStatics/PlaySoundAtLocation/1/index.html) Pass hit info and refrence to Sound Asset.
		
	* Use FCollisionQueryParams to define how the Trace behaves.
		
		* Set Params as seen below. 
			
			```c++

			FCollisionQueryParams FireTraceParams;
			FireTraceParams.bTraceComplex = false;
			FireTraceParams.bTraceAsyncScene = true;
			FireTraceParams.bReturnPhysicalMaterial = true;
			
			```

3. Create A target blueprint
	* Add a Box Collision Component as root
	* Add a Static Mesh. For this sample use a cube mesh
	* Use [TimeLine](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/Timelines/) to Make move back and Forward.
		* Follow this [Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/UserGuide/Timelines/Examples/OpeningDoors/index.html) 
	* Apply Material - Set this on the instances of the targets.

4. Create a wood/stone and metal materials with Phys Materials, Follow this [Physical Materials User Guide
](https://docs.unrealengine.com/latest/INT/Engine/Physics/PhysicalMaterials/PhysMatUserGuide/#creation)
	* Apply to Material as seen [here](https://docs.unrealengine.com/latest/INT/Engine/Physics/PhysicalMaterials/PhysMatUserGuide/#material)

5. Add transluent barrier - As Seen in SpikePe1

6. Add Targets To world - Change each target to have diffrent materials that we created above.


### What we found out

Skill: How do we use physics from C++?
* That Params are used to give information to Collisions and hit info.


### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.